from .train import LitNNSimulator
from torchvision.io import read_image
import torch as th
import numpy as np
import matplotlib.pyplot as plt

model = LitNNSimulator.load_from_checkpoint(
    "models/nnsim.ckpt", freq_list_txt="freq.txt"
)
model.eval()

img_name = "4613_6"
spec_name = img_name.split("_")[0]
thick = int(img_name.split("_")[1]) - 1

image = read_image("_dataset/imgs/{}.png".format(img_name)).unsqueeze(0)/255
thickness = th.eye(16)[thick].float().unsqueeze(0)

y = model.spectrum(image, thickness).detach().numpy()[0]

S11_0 = y[0] + 1j*y[1]
S11_90 = y[2] + 1j*y[3]
S21_0 = y[4] + 1j*y[5]
S21_90 = y[6] + 1j*y[7]

spec = np.load("_dataset/spectrums/{}.npy".format(spec_name))

S11_0_true = spec[0] + 1j*spec[1]
S11_90_true = spec[2] + 1j*spec[3]
S21_0_true = spec[4] + 1j*spec[5]
S21_90_true = spec[6] + 1j*spec[7]

f = np.linspace(0.5, 1.0, 250)

plt.figure()
plt.plot(f, np.abs(S21_0) ** 2, 'b-', label='$T_{∥}$')
plt.plot(f, np.abs(S21_90) ** 2, 'b--', label='$T_{⊥}$')
plt.plot(f, np.abs(S11_0) ** 2, 'r-', label='$R_{∥}$')
plt.plot(f, np.abs(S11_90) ** 2, 'r--', label='$R_{⊥}$')
plt.ylim(0, 1)
plt.xlabel("Frequency [THz]")
plt.ylabel("Transmittance / Reflectance [-]")
plt.legend()
plt.savefig('{}_amp_sim.png'.format(img_name))
plt.close()

plt.figure()
plt.plot(f, (np.angle(S21_0)+np.pi) / (2*np.pi), 'b-', label='$T_{∥}$')
plt.plot(f, (np.angle(S21_90)+np.pi) / (2*np.pi), 'b--', label='$T_{⊥}$')
plt.plot(f, (np.angle(S11_0)+np.pi) / (2*np.pi), 'r-', label='$R_{∥}$')
plt.plot(f, (np.angle(S11_90)+np.pi) / (2*np.pi), 'r--', label='$R_{⊥}$')
plt.ylim(0, 1)
plt.xlabel("Frequency [THz]")
plt.ylabel("Phase-shift /2$\pi$ [-]")
plt.legend()
plt.savefig('{}_angle_sim.png'.format(img_name))
plt.close()

plt.figure()
plt.plot(f, np.abs(S21_0_true) ** 2, 'b-', label='$T_{∥}$')
plt.plot(f, np.abs(S21_90_true) ** 2, 'b--', label='$T_{⊥}$')
plt.plot(f, np.abs(S11_0_true) ** 2, 'r-', label='$R_{∥}$')
plt.plot(f, np.abs(S11_90_true) ** 2, 'r--', label='$R_{⊥}$')
plt.ylim(0, 1)
plt.xlabel("Frequency [THz]")
plt.ylabel("Transmittance / Reflectance [-]")
plt.legend()
plt.savefig('{}_amp_true.png'.format(img_name))
plt.close()

plt.figure()
plt.plot(f, (np.angle(S21_0_true)+np.pi) / (2*np.pi), 'b-', label='$T_{∥}$')
plt.plot(f, (np.angle(S21_90_true)+np.pi) / (2*np.pi), 'b--', label='$T_{⊥}$')
plt.plot(f, (np.angle(S11_0_true)+np.pi) / (2*np.pi), 'r-', label='$R_{∥}$')
plt.plot(f, (np.angle(S11_90_true)+np.pi) / (2*np.pi), 'r--', label='$R_{⊥}$')
plt.ylim(0, 1)
plt.xlabel("Frequency [THz]")
plt.ylabel("Phase-shift /2$\pi$ [-]")
plt.legend()
plt.savefig('{}_angle_true.png'.format(img_name))
plt.close()
