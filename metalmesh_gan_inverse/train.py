import yaml
import torch as th
from torch.nn import functional as F
from torch.utils.data import DataLoader
import pytorch_lightning as pl
from .model import Generator, Critic
from .dataset import GANDataset
import torchvision
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
import copy
import argparse
from nn_simulator_v2.train import LitNNSimulator
from torch.utils.data import random_split


class LitGAN(pl.LightningModule):

    def __init__(
        self, nnsim, num_c_train=5, g_learning_rate=1e-4,
        c_learning_rate=1e-5, **kwargs
    ):
        super().__init__()
        self.generator = Generator()
        self.critic = Critic()
        self.critic_weights = Critic()
        self.nnsim = LitNNSimulator().load_from_checkpoint(nnsim)
        self.eval()
        self.save_hyperparameters("g_learning_rate")
        self.save_hyperparameters("c_learning_rate")
        self.save_hyperparameters("num_c_train")

    def forward(self, x, noise):
        image, thickness = self.generator(x, noise)
        # make symmetry images.
        image_batch = th.zeros(x.size()[0], 1, 32, 32).type_as(x)
        for i in range(x.size()[0]):
            image_unit = image[i, 0, 32//2:, 32//2:]
            image_tri = th.tril(image_unit)
            image_unit = image_tri + image_tri.T - \
                th.diag(th.diagonal(image_unit))
            image_batch[i, 0, :32//2, :32//2] += th.rot90(image_unit, k=2)
            image_batch[i, 0, :32//2, 32//2:] += th.rot90(image_unit, k=1)
            image_batch[i, 0, 32//2:, 32//2:] += image_unit
            image_batch[i, 0, 32//2:, :32//2] += th.rot90(image_unit, k=-1)
        return image_batch, thickness

    def adversarial_loss(self, y_hat, y):
        return F.binary_cross_entropy(y_hat, y)

    def nnsim_loss(self, image, thickness, z):
        y = self.nnsim(image, thickness)
        loss = F.l1_loss(z, y)
        return loss

    def loss_recon(self, real, fake, thickness, thickness_fake):
        loss_image = F.l1_loss(fake, real)
        loss_thickness = F.l1_loss(thickness_fake, thickness)
        return loss_image + loss_thickness

    def training_step(self, batch, batch_idx, optimizer_idx):
        real, thickness, z = batch
        # z = self.nnsim(real, thickness)
        noise = th.randn(real.size(0), 256).type_as(real)
        valid = th.ones(real.size(0), 1).type_as(real)
        invalid = th.zeros(real.size(0), 1).type_as(real)

        # train critic.
        if optimizer_idx < self.hparams.num_c_train:

            if optimizer_idx == 1:
                self.critic_weights = copy.deepcopy(self.critic)

            real_loss = self.adversarial_loss(
                self.critic(real),
                valid
            )

            fake_loss = self.adversarial_loss(
                self.critic(self(z, noise)[0].detach()),
                invalid
            )

            loss = (real_loss + fake_loss) / 2

            self.log('critic_loss', loss, prog_bar=True)

            return loss

        # train generator.
        if optimizer_idx == self.hparams.num_c_train:

            fake, thickness_fake = self(z, noise)
            sample_imgs = fake[:6]
            grid = torchvision.utils.make_grid(sample_imgs)
            torchvision.utils.save_image(grid, "test/img.png")

            loss_a = self.adversarial_loss(
                self.critic(fake),
                valid
            )
            loss_nnsim = self.nnsim_loss(
                fake,
                thickness_fake,
                z
            )
            loss_recon = self.loss_recon(real, fake, thickness, thickness_fake)
            loss = loss_a + loss_nnsim*1e2 + loss_recon

            self.log('generator_loss', loss, prog_bar=True)
            self.log('loss_nnsim', loss_nnsim, prog_bar=True)
            self.log('loss_recon', loss_recon, prog_bar=True)

            self.critic.load(self.critic_weights)  # unroll

            return loss

    def validation_step(self, val_batch, batch_idx):
        real, thickness, z = val_batch
        # z = self.nnsim(real, thickness)
        noise = th.randn(real.size(0), 256).type_as(real)
        fake, thickness_fake = self(z, noise)
        loss_nnsim = self.nnsim_loss(
            fake,
            thickness_fake,
            z
        )
        loss_recon = self.loss_recon(real, fake, thickness, thickness_fake)
        self.log('val_loss_nnsim', loss_nnsim, prog_bar=True)
        self.log('val_loss_recon', loss_recon, prog_bar=True)

    def test_step(self, val_batch, batch_idx):
        real, thickness, z = val_batch
        # z = self.nnsim(real, thickness)
        noise = th.randn(real.size(0), 256).type_as(real)
        fake, thickness_fake = self(z, noise)
        loss_nnsim = self.nnsim_loss(
            fake,
            thickness_fake,
            z
        )
        loss_recon = self.loss_recon(real, fake, thickness, thickness_fake)
        self.log('test_loss_nnsim', loss_nnsim, prog_bar=True)
        self.log('test_loss_recon', loss_recon, prog_bar=True)

    def configure_optimizers(self):
        opt_g = th.optim.Adam(
            self.generator.parameters(), lr=self.hparams.g_learning_rate
        )
        opt_c = th.optim.Adam(
            self.critic.parameters(), lr=self.hparams.c_learning_rate
        )
        return [opt_c for _ in range(self.hparams.num_c_train)]+[opt_g], []


if __name__ == "__main__":

    pl.seed_everything(0)
    th.manual_seed(0)

    parser = argparse.ArgumentParser(description='train GANs')
    parser.add_argument('nnsim_path')
    parser.add_argument('dataset_dir')
    parser.add_argument(
        '--hparams', default="hparams/metalmesh_gan_inverse_hparams.yml"
    )
    parser.add_argument('--resume', default=None)
    args = parser.parse_args()

    with open(args.hparams, 'r') as yml:
        config = yaml.safe_load(yml)

    batchsize = config["batchsize"]
    num_c_train = config["num_c_train"]
    g_learning_rate = config["g_learning_rate"]
    c_learning_rate = config["c_learning_rate"]
    max_epochs = config["max_epochs"]

    test_split_rate = config["test_rate"]
    valid_split_rate = config["valid_rate"] + test_split_rate

    dataset = GANDataset(root=args.dataset_dir)
    train_num = int(len(dataset)*(1.0 - valid_split_rate))
    train, val = random_split(
        dataset, [train_num, len(dataset)-train_num],
        generator=th.Generator().manual_seed(0)
    )
    test_num = int(len(val)*test_split_rate / valid_split_rate)
    val, test = random_split(
        val, [len(val)-test_num, test_num],
        generator=th.Generator().manual_seed(0)
    )

    batchsize = config["batchsize"]
    train_loader = DataLoader(train, batch_size=batchsize,
                              shuffle=True, num_workers=4)
    val_loader = DataLoader(val, batch_size=batchsize,
                            shuffle=False, num_workers=4)
    test_loader = DataLoader(test, batch_size=batchsize,
                             shuffle=False, num_workers=4)

    checkpoint_callback = ModelCheckpoint(
        dirpath='metalmesh_gan_inverse_checkpoints/', save_last=True,
        monitor="critic_loss"
    )
    gpu = 0
    if th.cuda.is_available():
        gpu = 1
    trainer = pl.Trainer(
        gpus=gpu, callbacks=[checkpoint_callback],
        resume_from_checkpoint=args.resume,
        max_epochs=max_epochs
    )
    gan = LitGAN(args.nnsim_path, num_c_train,
                 g_learning_rate, c_learning_rate)
    trainer.fit(gan, train_loader, val_loader)
    trainer.test(dataloaders=test_loader, ckpt_path=None)
