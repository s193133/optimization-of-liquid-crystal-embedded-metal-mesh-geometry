import torch as th
from torch.utils.data import Dataset
from torchvision.io import read_image
import numpy as np
import json
import os.path as path


class GANDataset(Dataset):

    def __init__(self, root):
        super().__init__()
        self.root = root
        with open(self.getpath("geometry_params.json")) as fp:
            self.geometry_params = json.load(fp)

    def __len__(self):
        return len(self.geometry_params)

    def __getitem__(self, index):
        image = read_image(
            self.getpath("imgs", self.geometry_params[index]["filename"])
        )/255
        thickness = th.eye(16)[self.geometry_params[index]["thickness"]-1]

        spectrum = np.load(self.getpath("spectrums", "{}.npy".format(index)))
        t_0 = spectrum[4] + 1j*spectrum[5]
        t_90 = spectrum[6] + 1j*spectrum[7]

        amp = np.stack([np.abs(t_0)**2, np.abs(t_90)**2])
        phase = (np.stack([np.angle(t_0), np.angle(t_90)]) + np.pi) / (2*np.pi)
        y = np.concatenate((amp, phase))

        return image, thickness, np.array(y, dtype=np.float32)

    def getpath(self, *x):
        return path.join(self.root, *x)


if __name__ == "__main__":
    dataset = GANDataset("dataset")
    img = dataset.__getitem__(0)
    print(img.shape)
