from .train import LitNNSimulator
from torchvision.io import read_image
import torch as th
import numpy as np
import matplotlib.pyplot as plt

model = LitNNSimulator.load_from_checkpoint(
    "nn_simulator_v2_checkpoints/last.ckpt"
)
model.eval()

image = read_image("dataset/imgs/0_6.png").unsqueeze(0)/255
thickness = th.eye(16)[10].float().unsqueeze(0)

y = model(image, thickness).detach().numpy()[0]

plt.figure()
plt.plot(y[0], 'b-', label='tran0')
plt.plot(y[1], 'b--', label='tran90')
plt.ylim(0, 1)
plt.xlabel("Freq (THz)")
plt.ylabel("Transmittance/Reflectance (-)")
plt.legend()
plt.savefig('amp.png')
plt.close()

plt.figure()
plt.plot(y[2], 'b-', label='tran0')
plt.plot(y[3], 'b--', label='tran90')
plt.ylim(0, 1)
plt.xlabel("Freq (THz)")
plt.ylabel("Phase (-)")
plt.legend()
plt.savefig('angle.png')
plt.close()
