import torch as th
import torch.nn as nn
import numpy as np


class CenterCrop(nn.Module):

    def __init__(self, target_shape):
        super().__init__()
        self.target_shape = target_shape

    def forward(self, tensor):
        shape = tensor.shape[-1]
        if shape == self.target_shape:
            return tensor
        diff = shape - self.target_shape
        crop_start = diff // 2
        crop_end = diff - crop_start
        return tensor[:, :, crop_start:-crop_end]


class Model(nn.Module):

    def __init__(self, out_shape):
        super().__init__()
        self.out_shape = out_shape

        self.feature = nn.Sequential(
            nn.Conv2d(1, 64, 5, stride=2),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64, 256, 5, stride=2),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 512, 5, stride=2),
            nn.BatchNorm2d(512),
            nn.ReLU(),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten()
        )

        self.fc = nn.Sequential(
            nn.Linear(512+16, 1024),
            nn.ReLU(),
            nn.Linear(1024, 64*35),
            nn.ReLU(),
        )

        def out_factory():
            layers = [
                nn.ConvTranspose1d(64, 32, 15, stride=2),
                nn.ReLU(),
                nn.Conv1d(32, 32, 15),
                nn.ReLU(),
                nn.ConvTranspose1d(32, 16, 15, stride=2),
                nn.ReLU(),
                nn.Conv1d(16, 16, 15),
                nn.ReLU(),
                nn.ConvTranspose1d(16, 8, 15, stride=2),
                nn.ReLU(),
                nn.Conv1d(8, 8, 15),
                nn.ReLU(),
                nn.ConvTranspose1d(8, 4, 15, stride=2),
                nn.ReLU(),
                nn.Conv1d(4, 4, 15),
                nn.ReLU(),
                CenterCrop(self.out_shape),
                nn.Conv1d(4, 1, 1),
                nn.ReLU()
            ]
            return nn.Sequential(*layers)

        self.out = nn.ModuleList([out_factory() for _ in range(4)])

    def forward(self, x, thickness):
        feature = th.cat([self.feature(x), thickness], dim=1)
        fc = self.fc(feature).view(-1, 64, 35)
        x = th.cat([out(fc) for out in self.out], dim=1)
        return x.clamp(0.0, 1.0)


if __name__ == "__main__":

    model = Model()
    dummy_img = th.randn(1, 1, 32, 32)
    dummy_thickness = th.eye(16)[0].unsqueeze(0).float()
    y = model(dummy_img, dummy_thickness)
    print(y.shape)
