from PIL import Image
import meep as mp
import numpy as np
import torch as th


C = 299_792_458


def load_mask(path, size):
    im = Image.open(path)
    im = im.resize((size[0], size[1]))
    im = im.convert('L')
    array = np.array(im) / 255
    return array


def arrange_medium(mask, width, height, thickness, a, epsilon):
    medium_list = []
    unit_cell = (width/a) / mask.shape[0]
    for x in range(mask.shape[0]):
        for y in range(mask.shape[1]):
            if mask[y][x] == 1:
                material = mp.Medium(epsilon_diag=epsilon)
                medium = mp.Block(
                    size=mp.Vector3(unit_cell, unit_cell, thickness/a),
                    center=mp.Vector3(
                        unit_cell*x-(width/a)/2+unit_cell/2,
                        (height/a)/2-unit_cell*y-unit_cell/2, 0
                    ),
                    material=material
                )
                medium_list.append(medium)

    return medium_list


def fresnel(freq, neffe, zeffe, d, backend=th):

    k2 = 2.0 * np.pi * freq * neffe / C

    Z1 = 1
    Z2 = zeffe
    Z3 = 1

    t12 = 2 * Z2 / (Z1 + Z2)
    t23 = 2 * Z3 / (Z2 + Z3)

    r12 = (Z2 - Z1) / (Z1 + Z2)
    r23 = (Z3 - Z2) / (Z2 + Z3)

    P2 = backend.exp(1j * k2 * d)

    t123 = (t12 * t23 * P2) / (1 + r12 * r23 * P2**2)
    r123 = (r12 + r23 * P2**2) / (1 + r12 * r23 * P2**2)

    return t123, r123
