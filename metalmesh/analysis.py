import torch as th
import numpy as np
import argparse
from .utils import load_mask, arrange_medium, C, fresnel
import meep as mp
from tqdm import tqdm
import json
import pickle
from .simulator import Simulation
import matplotlib.pyplot as plt
import datetime
import os


def fit(S11_, S21_, d_, freq_, err_th=1e-6, itr=5e4):

    n = th.ones(2, freq_.shape[0], requires_grad=True)
    k = th.ones(2, freq_.shape[0], requires_grad=True)
    imp_real = th.ones(2, freq_.shape[0], requires_grad=True)
    imp_img = th.ones(2, freq_.shape[0], requires_grad=True)

    optim = th.optim.Adam([n, k, imp_real, imp_img], lr=1e-3)

    for _ in tqdm(range(int(itr))):

        optim.zero_grad()

        freq = th.tensor(freq_)
        d = th.tensor(d_)
        k0 = 2*np.pi*freq / C
        S11 = th.tensor(S11_)
        S21 = th.tensor(S21_) * th.exp(1j * k0 * d)
        imp = th.abs(imp_real) + 1j*imp_img
        n2 = th.abs(n) + 1j*th.abs(k)

        t123, r123 = fresnel(freq, n2, imp, d, backend=th)

        loss = th.nn.MSELoss()(r123.real, S11.real) \
            + th.nn.MSELoss()(r123.imag, S11.imag) \
            + th.nn.MSELoss()(t123.real, S21.real) \
            + th.nn.MSELoss()(t123.imag, S21.imag)

        if loss.item() < err_th:
            break

        loss.backward()

        optim.step()

    return (
        (th.abs(n) + 1j*th.abs(k)).detach().numpy(),
        (th.abs(imp_real) + 1j*imp_img).detach().numpy()
    )


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('image', type=str)
    parser.add_argument('thickness', type=int)

    args = parser.parse_args()

    mp.verbosity(0)

    with open("sim_param.json", "r") as fp:
        sim_param = json.load(fp)

    a = sim_param["a"]
    resolution = sim_param["resolution"]
    sx = sy = sim_param["sx_sy"]
    sz = sim_param["sz"]
    field_size = (sx, sy, sz)
    f_center = sim_param["f_center"]
    f_width = sim_param["f_width"]
    source_center = sim_param["source_center"]
    pml_width = sim_param["pml_width"]
    monitor_position = sim_param["monitor_position"]
    simulation_time = sim_param["simulation_time"]

    with open("refs/tran_incidnet.pickle", mode='rb') as fp:
        tran_incidnet = pickle.load(fp)
    with open("refs/refl_straight.pickle", mode='rb') as fp:
        refl_straight = pickle.load(fp)

    def simulator_factory(medium, medium_width):
        return Simulation(
            f_center, f_width, source_center,
            field_size, resolution, pml_width,
            medium, medium_width, monitor_position,
            sim_time=simulation_time, n_freq=500, a=a
        )

    def medium_factory(thickness, medium0, medium90):
        medium = {
            0: [
                mp.Block(
                    size=mp.Vector3(mp.inf, mp.inf, thickness),
                    center=mp.Vector3(0, 0, 0),
                    material=mp.perfect_electric_conductor
                ),
                *medium0
            ],
            90: [
                mp.Block(
                    size=mp.Vector3(mp.inf, mp.inf, thickness),
                    center=mp.Vector3(0, 0, 0),
                    material=mp.perfect_electric_conductor
                ),
                *medium90
            ],
        }
        return medium

    medium_width = args.thickness * a / resolution
    mask = load_mask(
        args.image,
        (32, 32)
    )
    medium0 = arrange_medium(
        mask, sx, sy, medium_width,
        a, mp.Vector3(1.69**2, 1.54**2, 1.54**2)
    )
    medium90 = arrange_medium(
        np.rot90(mask), sx, sy, medium_width,
        a, mp.Vector3(1.54**2, 1.69**2, 1.54**2)
    )

    medium = medium_factory(medium_width / a, medium0, medium90)
    simulator = simulator_factory(medium, medium_width)
    with open("refs/refl_incident_{}.pickle".format(args.thickness), mode='rb') as fp:
        refl_incident = pickle.load(fp)

    simulator.run(tran_incidnet, refl_incident, refl_straight)
    n, imp = fit(simulator.S11, simulator.S21, medium_width, simulator.freq)
    S21_fresnel, S11_fresnel = fresnel(
        simulator.freq, n, imp, medium_width, backend=np)

    deltaN = np.abs(n[0].real - n[1].real)

    S21_0_amp = np.abs(simulator.S21[0]) ** 2
    S21_90_amp = np.abs(simulator.S21[1]) ** 2
    S11_0_amp = np.abs(simulator.S11[0]) ** 2
    S11_90_amp = np.abs(simulator.S11[1]) ** 2
    S21_0_angle = np.angle(simulator.S21[0])
    S21_90_angle = np.angle(simulator.S21[1])
    S11_0_angle = np.angle(simulator.S11[0])
    S11_90_angle = np.angle(simulator.S11[1])

    S21_0_amp_fresnel = np.abs(S21_fresnel[0]) ** 2
    S21_90_amp_fresnel = np.abs(S21_fresnel[1]) ** 2
    S11_0_amp_fresnel = np.abs(S11_fresnel[0]) ** 2
    S11_90_amp_fresnel = np.abs(S11_fresnel[1]) ** 2
    S21_0_angle_fresnel = np.angle(S21_fresnel[0])
    S21_90_angle_fresnel = np.angle(S21_fresnel[1])
    S11_0_angle_fresnel = np.angle(S11_fresnel[0])
    S11_90_angle_fresnel = np.angle(S11_fresnel[1])

    phaseshift_diff = np.abs(
        S21_0_angle - S21_90_angle
    )

    max_phaseshift_index = phaseshift_diff.argmax()
    max_phaseshift_freq_THz = simulator.freq[max_phaseshift_index]*1e-12
    max_phaseshift = phaseshift_diff[max_phaseshift_index]

    trans0_at_max_phaseshift_freq_THz = S21_0_amp_fresnel[max_phaseshift_index]
    trans90_at_max_phaseshift_freq_THz = S21_90_amp_fresnel[max_phaseshift_index]

    max_deltaN_index = deltaN.argmax()
    max_deltaN_freq_THz = simulator.freq[max_deltaN_index]*1e-12
    max_deltaN = deltaN[max_deltaN_index]

    dir = "analysis/{}".format(
        datetime.datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
    )
    os.makedirs(dir)

    with open("{}/summary.txt".format(dir), "w") as fp:
        fp.write("max_phaseshift_freq_THz: {}\n".format(
            max_phaseshift_freq_THz))
        fp.write("max_phaseshift: {}\n".format(
            max_phaseshift))
        fp.write("trans0_at_max_phaseshift_freq: {}\n".format(
            trans0_at_max_phaseshift_freq_THz))
        fp.write("trans90_at_max_phaseshift_freq: {}\n".format(
            trans90_at_max_phaseshift_freq_THz))
        fp.write("max_deltaN_freq_THz: {}\n".format(
            max_deltaN_freq_THz))
        fp.write("max_deltaN: {}\n".format(
            max_deltaN))

    plt.figure()
    plt.plot(simulator.freq*1e-12, phaseshift_diff / (2*np.pi))
    plt.xlim(0.5, 1.0)
    plt.xlabel("Frequency [THz]")
    plt.ylabel("$\Delta$ Phase-shift /2$\pi$ [-]")
    plt.savefig('{}/phase_delta.png'.format(dir))
    plt.close()

    plt.figure()
    plt.plot(simulator.freq*1e-12, S21_0_amp, 'b-', label='$T_{∥}$')
    plt.plot(simulator.freq*1e-12, S21_90_amp, 'b--', label='$T_{⊥}$')
    plt.plot(simulator.freq*1e-12, S11_0_amp, 'r-', label='$R_{∥}$')
    plt.plot(simulator.freq*1e-12, S11_90_amp, 'r--', label='$R_{⊥}$')
    plt.xlim(0.5, 1.0)
    plt.ylim(0, 1)
    plt.xlabel("Frequency [THz]")
    plt.ylabel("Transmittance / Reflectance [-]")
    plt.legend()
    plt.savefig('{}/amp.png'.format(dir))
    plt.close()

    plt.figure()
    plt.plot(simulator.freq*1e-12, (S21_0_angle + np.pi) /
             (2*np.pi), 'b-', label='$T_{∥}$')
    plt.plot(simulator.freq*1e-12, (S21_90_angle + np.pi) /
             (2*np.pi), 'b--', label='$T_{⊥}$')
    plt.plot(simulator.freq*1e-12, (S11_0_angle + np.pi) /
             (2*np.pi), 'r-', label='$R_{∥}$')
    plt.plot(simulator.freq*1e-12, (S11_90_angle + np.pi) /
             (2*np.pi), 'r--', label='$R_{⊥}$')
    plt.xlim(0.5, 1.0)
    plt.xlabel("Frequency [THz]")
    plt.ylabel("Phase-shift /2$\pi$ [-]")
    plt.legend()
    plt.savefig('{}/phase.png'.format(dir))
    plt.close()

    plt.figure()
    plt.plot(simulator.freq*1e-12, deltaN)
    plt.xlim(0.5, 1.0)
    plt.xlabel("Frequency [THz]")
    plt.ylabel("$\Delta n$ [-]")
    plt.savefig('{}/deltaN.png'.format(dir))
    plt.close()

    # plt.figure()
    # plt.plot(simulator.freq*1e-12, n[0].real, 'r-', label='n0')
    # plt.plot(simulator.freq*1e-12, n[0].imag, 'r--', label='k0')
    # plt.plot(simulator.freq*1e-12, n[1].real, 'b-', label='n90')
    # plt.plot(simulator.freq*1e-12, n[1].imag, 'b--', label='k90')
    # plt.xlim(0.5, 1.0)
    # plt.xlabel("Freq (THz)")
    # plt.ylabel("$\hat{n}$")
    # plt.legend()
    # plt.savefig('{}/n.png'.format(dir))
    # plt.close()

    # plt.figure()
    # plt.plot(simulator.freq*1e-12, S21_0_amp_fresnel, 'r-', label='tran0')
    # plt.plot(simulator.freq*1e-12, S21_90_amp_fresnel, 'r--', label='tran90')
    # plt.plot(simulator.freq*1e-12, S11_0_amp_fresnel, 'b-', label='refl0')
    # plt.plot(simulator.freq*1e-12, S11_90_amp_fresnel, 'b--', label='refl90')
    # plt.xlim(0.5, 1.0)
    # plt.ylim(0, 1)
    # plt.xlabel("Freq (THz)")
    # plt.ylabel("Transmittance/Reflectance (-)")
    # plt.legend()
    # plt.savefig('{}/amp_fresnel.png'.format(dir))
    # plt.close()

    # plt.figure()
    # plt.plot(simulator.freq*1e-12, S21_0_angle_fresnel, 'r-', label='tran0')
    # plt.plot(simulator.freq*1e-12, S21_90_angle_fresnel, 'r--', label='tran90')
    # plt.plot(simulator.freq*1e-12, S11_0_angle_fresnel, 'b-', label='refl0')
    # plt.plot(simulator.freq*1e-12, S11_90_angle_fresnel, 'b--', label='refl90')
    # plt.xlim(0.5, 1.0)
    # plt.xlabel("Freq (THz)")
    # plt.ylabel("Phase shift (rad)")
    # plt.legend()
    # plt.savefig('{}/phase_fresnel.png'.format(dir))
    # plt.close()

    with open('{}/data.csv'.format(dir), 'w') as f:

        f.write(
            'freq, \
                    S11_0.real,\
                    S11_0.imag,\
                    S11_90.real,\
                    S11_90.imag,\
                    S21_0.real,\
                    S21_0.imag,\
                    S21_90.real,\
                    S21_90.imag,\
                    z_0.real,\
                    z_0.imag,\
                    z_90.real,\
                    z_90.imag,\
                    n_0.real,\
                    n_0.imag,\
                    n_90.real,\
                    n_90.imag,\
                    \n'
        )

        for i in range(len(simulator.freq)):
            f.write(
                '{}, \
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        {},\
                        \n'.format(
                    simulator.freq[i],
                    simulator.S11[0, i].real,
                    simulator.S11[0, i].imag,
                    simulator.S11[1, i].real,
                    simulator.S11[1, i].imag,
                    simulator.S21[0, i].real,
                    simulator.S21[0, i].imag,
                    simulator.S21[1, i].real,
                    simulator.S21[1, i].imag,
                    imp[0, i].real,
                    imp[0, i].imag,
                    imp[1, i].real,
                    imp[1, i].imag,
                    n[0, i].real,
                    n[0, i].imag,
                    n[1, i].real,
                    n[1, i].imag
                )
            )
