from .train import LitGAN
from nn_simulator import nn_simulator
import torch as th
import numpy as np
import matplotlib.pyplot as plt
import torchvision
import argparse
import datetime
import json
import os
from tqdm import tqdm


def optimize(
    nnsim, gan,
    lr, itr,
    save_best=False,
    freq_index=None,
    alpha=1, beta=1, gamma=1
):

    z = th.randn(1, 64).requires_grad_(True)
    thickness_weight = th.eye(16)[0].unsqueeze(0).requires_grad_(True)

    optim = th.optim.Adam([
        {'params': z},
        {'params': thickness_weight}
    ], lr=lr)

    best_loss = 0
    best = None

    for _ in tqdm(range(itr)):

        optim.zero_grad()

        image = gan(z)
        thickness = th.softmax(thickness_weight, dim=-1)

        n, r, T = extract(nnsim, image, thickness)

        if freq_index is None:
            phase, index = th.max(r*alpha + n*gamma, dim=-1)
            T_p = T[index]*beta
            loss = T_p + phase
            loss = -1 * loss
        else:
            loss = -1 * (r*alpha + T*beta + n*gamma)[freq_index]
            index = freq_index

        if loss < best_loss and save_best:
            best_loss = loss
            best = {
                "loss": loss,
                "thickness": thickness[0].clone(),
                "image": image[0].clone(),
                "T": T[index],
                "r": r[index],
                "n": n[index]
            }

        # print(T[index].item(), r[index].item(),
        #       n[index].item(), nnsim.freq[index], loss.item())

        loss.backward()

        optim.step()

    if save_best:
        return best

    return {
        "loss": loss,
        "thickness": thickness[0].clone(),
        "image": image[0].clone(),
        "T": T[index],
        "r": r[index],
        "n": n[index]
    }


def extract(nnsim, image, thickness):
    y = nnsim(image, thickness)[0]
    S = nnsim.spectrum(image, thickness)[0]
    S21_0 = S[4] + 1j*S[5]
    S21_90 = S[6] + 1j*S[7]
    n = (y[0] - y[1]).abs()
    r = th.abs(th.angle(S21_0) - th.angle(S21_90))
    T = th.abs(S21_0)**2 + th.abs(S21_90)**2
    return n, r/(2*np.pi), T/2


def treatment(image, thickness):
    image = th.where(image >= 0.5, 1.0, 0.0)
    thickness = th.eye(16)[th.max(thickness, dim=-1)[1]]
    return image, thickness


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('nnsim_model')
    parser.add_argument('gan_model')
    parser.add_argument('--alpha', default=1, type=float)
    parser.add_argument('--beta', default=1, type=float)
    parser.add_argument('--gamma', default=0, type=float)
    parser.add_argument('--lr', default=0.1, type=float)
    parser.add_argument('--itr', default=100, type=int)
    parser.add_argument('--freq_index', default=None, type=int)
    parser.add_argument('--save_best', default=True, type=bool)
    parser.add_argument('--seed', default=0, type=int)
    args = parser.parse_args()

    th.manual_seed(args.seed)

    nnsim = nn_simulator(args.nnsim_model, "freq.txt")
    nnsim.eval()
    gan = LitGAN(nnsim=nnsim).load_from_checkpoint(
        args.gan_model
    )
    gan.eval()

    res = optimize(
        nnsim, gan,
        args.lr, args.itr, args.save_best,
        args.freq_index,
        args.alpha, args.beta, args.gamma
    )
    image, thickness = treatment(res["image"], res["thickness"])
    n, r, T = extract(nnsim, image.unsqueeze(0), thickness.unsqueeze(0))
    if args.freq_index is None:
        _, index = th.max(r, dim=-1)
    else:
        index = args.freq_index
    thickness_index = (th.max(thickness, dim=-1)[1]+1).item()
    # print(n[index], r[index], T[index], thickness_index, nnsim.freq[index])
    y = nnsim(
        image.unsqueeze(0),
        thickness.unsqueeze(0)
    ).detach().cpu().numpy()[0]

    dir = "optimize/{}/".format(
        datetime.datetime.now().strftime('%Y-%m-%d.%H:%M:%S')
    )
    os.makedirs(dir)

    torchvision.utils.save_image(image, dir+"image.png")

    freq = nnsim.fresnel.freq / 1e12

    plt.figure()
    plt.plot(freq, np.abs(y[0]), 'r-', label='n0')
    plt.plot(freq, np.abs(y[1]), 'b-', label='n90')
    plt.plot(freq, np.abs(np.abs(y[0]) -
             np.abs(y[1])), 'g-', label='$\Delta n$')
    plt.xlabel("Freq (THz)")
    plt.ylabel("n")
    plt.legend()
    plt.savefig(dir+'n.png')
    plt.close()

    y = nnsim.spectrum(
        image.unsqueeze(0),
        thickness.unsqueeze(0)
    ).detach().cpu().numpy()[0]

    S11_0 = y[0] + 1j*y[1]
    S11_90 = y[2] + 1j*y[3]
    S21_0 = y[4] + 1j*y[5]
    S21_90 = y[6] + 1j*y[7]

    plt.figure()
    plt.plot(freq, np.abs(S21_0) ** 2, 'r-', label='tran0')
    plt.plot(freq, np.abs(S11_0) ** 2, 'b-', label='refl0')
    plt.plot(freq, np.abs(S21_90) ** 2, 'r--', label='tran90')
    plt.plot(freq, np.abs(S11_90) ** 2, 'b--', label='refl90')
    plt.xlabel("Freq (THz)")
    plt.ylabel("Transmittance/Reflectance (-)")
    plt.legend()
    plt.savefig(dir+'amp.png')
    plt.close()

    plt.figure()
    plt.plot(freq, np.angle(S21_0), 'r-', label='tran0')
    plt.plot(freq, np.angle(S11_0), 'b-', label='refl0')
    plt.plot(freq, np.angle(S21_90), 'r--', label='tran90')
    plt.plot(freq, np.angle(S11_90), 'b--', label='refl90')
    plt.xlabel("Freq (THz)")
    plt.ylabel("Transmittance/Reflectance (-)")
    plt.legend()
    plt.savefig(dir+'angle.png')
    plt.close()

    with open(dir+"params.json", "w") as fp:
        json.dump(
            {
                "lr": args.lr,
                "itr": args.itr,
                "save_best": args.save_best,
                "alpha": args.alpha,
                "beta": args.beta,
                "gamma": args.gamma,
                "freq_index": args.freq_index,
                "thickness x 8um": thickness_index,
                "seed": args.seed
            }, fp
        )
