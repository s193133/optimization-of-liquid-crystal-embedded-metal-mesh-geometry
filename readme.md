# Requirements

* git
* [Docker](http://docs.docker.jp/engine/installation/)

# Usage

## Dockerイメージの作成

### 1. Clone this repository

```
git clone https://s193133@bitbucket.org/s193133/optimization-of-liquid-crystal-embedded-metal-mesh-geometry.git metalmesh
```

### 2. cd

```
cd metalmesh
```

### 3. Build docker image

Docker imageのビルドは初回のみで良い。

```
docker build --pull --rm -f "Dockerfile" -t metalmesh:latest "."
```

### 4. Run docker container

```
docker run -it -v (pwd):/home/meep metalmesh:latest
```

## データセットの作成

データセットは[ここ](https://drive.google.com/file/d/1Z2WnI7-iFxsVKYPr8WXVcmG-lLPBo7LA/view?usp=sharing)からダウンロードできる。
解凍してdataset/を置き換える。

### 1. 画像データの作成

dataset/imgsに画像データが作成される。

```
python3 -m metalmesh.generate_image
```

### 2. リファレンス波の測定

refs/にリファレンス波データが作成される。

[ここ](https://drive.google.com/file/d/1cUyJyIizGuo9CgVvmcczOv2ZOa_Tm6kQ/view?usp=sharing)からダウンロードしてrefs/を置き換えても良い。

```
python3 -m metalmesh.reference
```

### 3. 透過波／反射波の測定

dataset/spectrumsに透過波／反射波データが作成される。

offset引数から始まり、num_sample引数サンプル分の測定を行う。
サンプルを分けて複数マシンで実行したい時に使う。
引数を指定しない場合は全てのサンプルを測定する。

```
python3 -m metalmesh.worker --offset [offset] --num_sample [num_sample]
```

## NN電磁界シミュレータの学習

学習済みモデルは[ここ](https://drive.google.com/drive/folders/1GCxNo8a5zjwBQQZNMXCD0QuSv2Mq1xfB?usp=sharing)からダウンロードできる。

resume引数に学習途中のモデルデータのパス(nn_simulator_checkpoints/last.ckpt)を渡すと、そのデータから学習を再開する。

```
python3 -m nn_simulator.train dataset --resume [model_path]
```

## GANの学習

学習済みモデルは[ここ](https://drive.google.com/drive/folders/1GCxNo8a5zjwBQQZNMXCD0QuSv2Mq1xfB?usp=sharing)からダウンロードできる。

resume引数に学習途中のモデルデータのパス(metalmesh_gan_checkpoints/last.ckpt)を渡すと、そのデータから学習を再開する。

```
python3 -m metalmesh_gan.train dataset --resume [model_path]
```

## 最適化

nnsim_model引数にNN電磁界シミュレータのモデル、gan_model引数にGANのモデルのパスを指定する。

alpha、betaまたはgammaは以下の損失Lに係る係数である。

![](https://latex.codecogs.com/gif.download?L%20%3D%20%5Calpha%20%5CDelta%20r%20+%20%5Cbeta%20%28T_%7B%5Cparallel%7D%20+%20T_%7B%5Cperp%7D%29%20+%20%5Cgamma%20%5CDelta%20n)

結果はoptimize/<日時>以下に保存される。


```
python3 -m metalmesh_gan.optimize <nnsim_model> <gan_model> --alpha [alpha] --beta [beta] --gamma [gamma] --save_best True
```

## 解析

最適化した形状をFDTDシミュレータで解析する。

image引数にパターン画像のパス、thickness引数にoptimize/<日時>/params.json内の"thickness x 8um"欄記載の整数値を指定する。

結果はanalysis/<日時>以下に保存される。

```
python3 -m metalmesh.analysis <image> <thickness>
```